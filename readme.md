```shell
dotnet ef dbcontext scaffold "Data Source=localhost;User ID=myuser;Password=mypassword;Initial Catalog=mydb;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;" Microsoft.EntityFrameworkCore.SqlServer -d -f -c MyDbContext --context-dir Data/Context -o Data/Models
```


### Read the doc.

```shell
dotnet ef dbcontext scaffold --help
```
